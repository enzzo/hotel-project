import requests
import pytest
from hotel_detail_parsing import hotel_detail_parsing as hotel_parsing

TEST_DATA = {

    'ar/salta-apart-c': {'hotel_type': 'Departamentos',
                         'hotel_name': 'DepartamentosTaruca Apart',
                         'address': 'España 262, 4400 Salta, Argentina',
                         'opinions': 'Fantástico',
                         'score': '9,2'},


    'nl/zoku-amsterdam': {'hotel_type': 'Aparthotel',
                          'hotel_name': 'AparthotelZoku Amsterdam',
                          'address': 'Weesperstraat 105, Centro de'
                          ' Ámsterdam, 1018 VN Ámsterdam, Países Bajos',
                          'opinions': 'Fantástico',
                          'score': '9,0'},


    'fr/apartments-du-louvre': {'hotel_type': 'Departamentos',
                                'hotel_name':
                                'DepartamentosApartments Du Louvre',
                                'address': "44-45 Rue de l'Arbre Sec, Louvre -"
                                " 1er distrito, 75001 París, Francia",
                                'opinions': 'Fantástico',
                                'score': '9,1'},


    'mx/hausuites-mexico-city': {'hotel_type': 'Aparthotel',
                                 'hotel_name': 'AparthotelHausuites',
                                 'address': 'Prolongacion Paseo De La Reforma '
                                 '1190, 05349 Ciudad de México, México',
                                 'opinions': 'Fabuloso',
                                 'score': '8,8'}
}


@pytest.mark.parametrize("page", [
    ('ar/salta-apart-c'),
    ('nl/zoku-amsterdam'),
    ('fr/apartments-du-louvre'),
    ('mx/hausuites-mexico-city')])
def test_html_parsing_from_file(page):
    fname = f'hotel_detail_parsing/tests/data/{page}.html'
    with open(fname, mode="r", encoding="utf-8") as fd:
        actual = hotel_parsing.parse_booking_page(fd.read())
        expected = TEST_DATA[page]
        assert actual == expected


@pytest.mark.parametrize("page", [
    ('ar/salta-apart-c'),
    ('nl/zoku-amsterdam'),
    ('fr/apartments-du-louvre'),
    ('mx/hausuites-mexico-city')])
def test_html_parsing_from_ure(page):
    url = f'https://www.booking.com/hotel/{page}.es-ar.html'
    actual = hotel_parsing.parse_booking_page(
        requests.get(url).content.decode("utf-8"))
    expected = TEST_DATA[page]
    assert actual == expected
