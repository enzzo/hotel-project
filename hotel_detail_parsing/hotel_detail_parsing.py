from bs4 import BeautifulSoup


def extract_text_by_class(soup, class_name):
    """
    scrape knowing the page and one class.

     Parameters
     ----------
     soup : Beautifulsoup
         page to parse
     class_name : String
         class to search in parse
     Returns
     -------
     string
         scraped element.

    """

    element_page = soup.find(class_=class_name).get_text(strip=True)
    return element_page


def parse_booking_page(content_of_page):
    """
    returns a dictionary with parameters from a string with html format.

    Parameters
    ----------
    content_of_page : string
         Contain html format from one webpage to hotel

    Returns
    -------
    dictionary
         contains the information scraped to content_of_page.

    """
    soup = BeautifulSoup(content_of_page, 'html.parser')

    hotel_info = {
        'hotel_type': extract_text_by_class(soup, "bui-badge"),
        'hotel_name': extract_text_by_class(soup, "fn"),
        'address': extract_text_by_class(soup, "hp_address_subtitle"),
        'opinions': extract_text_by_class(soup, "bui-review-score__title"),
        'score': extract_text_by_class(soup, "bui-review-score__badge")
    }
    return hotel_info
