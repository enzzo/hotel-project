import setuptools


setuptools.setup(name='hotel_detail_parsing',
                 version='0.1.0',
                 packages=setuptools.find_packages(),
                 install_requires=['requests==2.25.0',
                                   'beautifulsoup4==4.9.3'])
